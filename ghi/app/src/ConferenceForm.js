import React, {useEffect, useState} from 'react';

function ConferenceForm(props) {

    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const [starts, setStarts]  = useState('');
    const handleStarts = (event) => {
        const value = event.target.value;
        setStarts(value);
    }

    const [ends, setEnds] = useState('');
    const handleEnds = (event) => {
        const value = event.target.value;
        setEnds(value);
    }


    const[location, setLocation] = useState('');
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const[description, setDescription] = useState('');
    const handleDescription = (event) => {
        const value = event.target.value;
        setDescription(value);
    }

    const[maxAttendees, setMaxAttendees] = useState('');
    const handleMaxAttendees = (event) => {
        const value = event.target.value;
        setMaxAttendees(value);
    }

    const[maxPresentations, setMaxPresentations] = useState('');
    const handleMaxPresentations = (event) => {
        const value = event.target.value;
        setMaxPresentations(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = location;
        console.log(data);

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
          const newConference = await response.json();
          console.log(newConference);

            setName('');
            setStarts('');
            setEnds('');
            setDescription('');
            setMaxPresentations('');
            setMaxAttendees('');
            setLocation('');
        }
      }

    const [locations, setLocations] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            console.log(data)
            setLocations(data.locations);
         }
        }

  useEffect(() => {
    fetchData();
  }, []);

  return(
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new conference</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
              <input onChange={handleNameChange} placeholder="Name" value={name} required type="text" name="name" id="name" className="form-control"/>
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-float mb-3">
                <input onChange={handleStarts} placeholder="Starts" value={starts} required type="date" name="starts" id="starts" className="form-control"/>
                <label htmlFor="starts">Starts</label>
              </div>
              <div className="form-float mb-3">
                <input onChange={handleEnds} placeholder="Ends" value={ends} required type="date" name="ends" id="ends" className="form-control"/>
                <label htmlFor="ends">Ends</label>
              </div>
              <div className="form-control mb-3">
                <input onChange={handleDescription} placeholder="Description" value={description} required type="text" name="description" id="description" className="form-control"/>
                <label htmlFor="ends">Description</label>
              </div>
              <div className="form-control mb-3">
                <input onChange={handleMaxPresentations} placeholder="Max presentations" value={maxPresentations} required type="number" name="max_presentations" id="max_presentations" className="form-control"/>
                <label htmlFor="max_presentations">Maximum presentations</label>
              </div>
              <div className="form-control mb-3">
                <input onChange={handleMaxAttendees} placeholder="Max attendees" value={maxAttendees} required type="number" name="max_attendees" id="max_attendees" className="form-control"/>
                <label htmlFor="max_attendees">Maximum attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocationChange} required id="location" value={location} name="location" className="form-select">
                  <option value="">Choose a location</option>
                  {locations.map((location, index) => {
                    return (
                        <option key={location.name + index} value={location.id}>
                        {location.name}
                        </option>
                    )
                  })}
                </select>
              </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ConferenceForm;
