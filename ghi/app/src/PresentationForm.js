import React, {useEffect, useState} from 'react';

function PresentationForm(props) {

    const [presenterName, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const [presenterEmail, setEmail] = useState('');
    const handleEmail = (event) => {
        const value = event.target.value;
        setEmail(value);
    }

    const [companyName, setCompany] = useState('');
    const handleCompanyName = (event) => {
        const value = event.target.value;
        setCompany(value);
    }

    const [title, setTitle] = useState('');
    const handleTitle = (event) => {
        const value = event.target.value;
        setTitle(value);
    }

    const [synopsis, setSynopsis] = useState('');
    const handleSynopsis = (event) => {
        const value = event.target.value;
        setSynopsis(value);
    }
    const [conference, setConference] = useState('');
    const handleConference = (event) => {
        const value = event.target.value;
        setConference(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.presenter_name = presenterName;
        data.presenter_email = presenterEmail;
        data.company_name = companyName;
        data.title = title;
        data.synopsis = synopsis;
        data.conference = conference;
        console.log(data);

        const presentationUrl = `http://localhost:8000${conference}presentations/`;
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
          const newPresentation = await response.json();
          console.log(newPresentation);

            setName('');
            setEmail('');
            setCompany('');
            setTitle('');
            setSynopsis('');
            setConference('');
        }
      }

    const [conferences, setConferences] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            console.log(data)
            setConferences(data.conferences);
        }
        }

    useEffect(() => {
      fetchData();
    }, []);


    return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} placeholder="Presenter name" required type="text" name="presenter_name" value={presenterName} id="presenter_name" className="form-control"/>
                <label htmlFor="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEmail} placeholder="Presenter email" required type="email" name="presenter_email" value={presenterEmail} id="presenter_email" className="form-control"/>
                <label htmlFor="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleCompanyName} placeholder="Company name" type="text" name="company_name" value={companyName} id="company_name" className="form-control"/>
                <label htmlFor="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleTitle} placeholder="Title" required type="text" name="title" value={title} id="title" className="form-control"/>
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea onChange={handleSynopsis} id="synopsis" rows="3" value={synopsis} name="synopsis" className="form-control"></textarea>
              </div>
              <div className="mb-3">
                <select onChange={handleConference} required name="conference" value={conference} id="conference" className="form-select">
                  <option value="">Choose a conference</option>
                  {conferences.map((conference, index) => {
                    return (
                        <option key={conference.href + index} value={conference.href}>
                        {conference.name}
                        </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    );
}

export default PresentationForm;
